.PHONY: clean

CFLAGS := -std=c++14 -O3 -Wall
LDFLAGS := -lpthread -lrdmacm -libverbs

APP := rbandwidth

all: $(APP)

rbandwidth: rbandwidth.cc
	g++ $^ -o $@ $(CFLAGS) $(LDFLAGS)

clean:
	rm -f $(APP)
