#include <getopt.h>
#include <stdio.h>
#include <string>
#include <netdb.h>
#include <chrono>
#include <thread>
#include <rdma/rsocket.h>

class Config {
 public:
  static Config *Get() {
    static Config instance;
    return &instance;
  }
  void set_is_server(bool is_server) { is_server_ = is_server; }
  bool is_server() const { return is_server_; }
  void set_host(const std::string &host) { host_ = host; }
  std::string host() const { return host_; }
  void set_buffer_size(size_t buffer_size) { buffer_size_ = buffer_size; }
  size_t buffer_size() const { return buffer_size_; }
  in_port_t port() const { return port_; }

 private:
  Config() {
    is_server_ = true;
    buffer_size_ = 65536;
    port_ = 1313;
  }
  bool is_server_;
  std::string host_;
  size_t buffer_size_;
  in_port_t port_;
};

void ShowUsage(const char *app) {
  fprintf(stdout, "Usage:\n");
  fprintf(stdout, "  %s           start a server and wait for connection\n", app);
  fprintf(stdout, "  %s <host>    connect to server at <host>\n", app);
  fprintf(stdout, "\nOptions:\n");
  fprintf(stdout, "  -b, --buffer-size=<size>  buffer size\n");
}

int ParseArgument(int argc, char *argv[]) {
  int err = 0;
  static struct option long_options[] = {// clang-format off
      {"help", required_argument, 0, 'h'},
      {"buffer-size", required_argument, 0, 'b'},
      {0, 0, 0, 0}
  };  // clang-format on
  while (1) {
    int option_index = 0, c;
    c = getopt_long(argc, argv, "hb:", long_options, &option_index);
    if (c == -1) break;
    switch (c) {
      case 'h': {
        ShowUsage(argv[0]);
      } break;
      case 'b': {
        int buffer_size = atoi(optarg);
        Config::Get()->set_buffer_size(buffer_size);
      } break;
      case '?':
      default:
        err = 1;
        goto out1;
    }
  }
  if (optind < argc) {
    if (optind + 1 != argc) {
      err = 1;
      goto out1;
    }
    std::string host = std::string(argv[optind]);
    Config::Get()->set_is_server(false);
    Config::Get()->set_host(host);
  }
out1:
  return err;
}

using namespace std::chrono;
using Clock = high_resolution_clock;

struct Counter {
  Counter() : cnt{0}, bytes{0}, tp{Clock::now()} {}
  void Add(size_t add) {
    bytes += add;
    if ((++cnt & 0xff) == 0xff) {
      auto now = Clock::now();
      if ((now - tp) >= seconds(1)) {
        duration<double> dura = now - tp;
        printf("Speed: %.6fMB/s\n", bytes / dura.count() / 1048576);
        bytes = 0;
        tp = now;
      }
    }
  }
  int cnt;
  size_t bytes;
  Clock::time_point tp;
};

void Process(int client_fd) {
  size_t buffer_size = Config::Get()->buffer_size();
  char *buffer = static_cast<char*>(malloc(buffer_size));
  ssize_t n;
  Counter counter;
  while ((n = rrecv(client_fd, buffer, buffer_size, 0)) >= 0) {
    counter.Add(n);
  }
  perror("recv");
  if (buffer) {
    free(buffer);
    buffer = nullptr;
  }
}

void StartServer() {
  int err = 0;
  int listener_fd = -1;

  struct sockaddr_storage client_addr;
  socklen_t addrlen = sizeof(client_addr);
  char host_str[NI_MAXHOST];
  char port_str[NI_MAXSERV];

  struct addrinfo hints, *res, *ap;
  memset(&hints, 0, sizeof(hints));
  hints.ai_flags = AI_PASSIVE | AI_ADDRCONFIG;
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = 0;
  err = getaddrinfo(nullptr, std::to_string(Config::Get()->port()).c_str(), &hints, &res);
  if (err) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(err));
    goto out2;
  }
  
  for (ap = res; ap; ap = ap->ai_next) {
    listener_fd = rsocket(ap->ai_family, ap->ai_socktype, ap->ai_protocol);
    if (listener_fd == -1)
      continue;
    if (0 == rbind(listener_fd, ap->ai_addr, ap->ai_addrlen))
      break;
    rclose(listener_fd);
  }
  if (!ap) {
    fprintf(stderr, "Could not bind\n");
    goto out2;
  }
  err = getnameinfo(ap->ai_addr, ap->ai_addrlen, host_str, sizeof(host_str), port_str, sizeof(port_str), NI_NUMERICHOST | NI_NUMERICSERV);
  if (err) {
    fprintf(stderr, "getnameinfo: %s\n", gai_strerror(err));
    goto out2;
  }
  printf("Bind to host: %s:%s\n", host_str, port_str);
  err = rlisten(listener_fd, 128);
  if (err) {
    perror("listen");
    goto out2;
  }
  for (;;) {
    int client_fd = raccept(listener_fd, reinterpret_cast<struct sockaddr*>(&client_addr), &addrlen);
    err = getnameinfo(reinterpret_cast<struct sockaddr*>(&client_addr), addrlen, host_str, sizeof(host_str), port_str, sizeof(port_str), NI_NUMERICHOST | NI_NUMERICSERV);
    if (err) {
      fprintf(stderr, "getnameinfo: %s\n", gai_strerror(err));
      goto out2;
    }
    printf("Connected from %s:%s\n", host_str, port_str);
    std::thread(Process, client_fd).detach();
  }
  if (listener_fd != -1) {
    rclose(listener_fd);
    listener_fd = -1;
  }
out2:
  if (res) {
    freeaddrinfo(res);
    res = nullptr;
  }
}

void StartClient() {
  int err = 0;
  int fd = -1;
  size_t buffer_size = Config::Get()->buffer_size();
  char* buffer = nullptr;
  Counter counter;
  struct addrinfo hints, *res, *ap;
  memset(&hints, 0, sizeof(hints));
  hints.ai_flags = AI_ADDRCONFIG;
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = 0;
  err = getaddrinfo(Config::Get()->host().c_str(), std::to_string(Config::Get()->port()).c_str(), &hints, &res);
  if (err) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(err));
    goto out3;
  }
  for (ap = res; ap; ap = ap->ai_next) {
    fd = rsocket(ap->ai_family, ap->ai_socktype, ap->ai_protocol);
    if (fd == -1) continue;
    if (0 == rconnect(fd, ap->ai_addr, ap->ai_addrlen))
      break;
    rclose(fd);
  }
  if (!ap) {
    perror("Could not connect");
    goto out3;
  }
  printf("Connect to %s:%d\n", Config::Get()->host().c_str(), Config::Get()->port());
  buffer = static_cast<char*>(malloc(buffer_size));
  for (;;) {
    size_t tlen = 0;
    ssize_t n;
    while (tlen < buffer_size && (n = rsend(fd, buffer + tlen, buffer_size, 0)) >= 0) {
      tlen += n;
      counter.Add(n);
    }
  }
out3:
  if (res) {
    freeaddrinfo(res);
    res = nullptr;
  }
  if (buffer) {
    free(buffer);
    buffer = nullptr;
  }
  if (fd >= 0) {
    rclose(fd);
    fd = -1;
  }
}

int main(int argc, char *argv[]) {
  if (ParseArgument(argc, argv)) {
    ShowUsage(argv[0]);
    return 1;
  }
  if (Config::Get()->is_server()) {
    StartServer();
  } else {
    StartClient();
  }
  return 0;
}
