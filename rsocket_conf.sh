#!/bin/bash

#       mem_default - default size of receive buffer(s)
#
#       wmem_default - default size of send buffer(s)
#
#       sqsize_default - default size of send queue
#
#       rqsize_default - default size of receive queue
#
#       inline_default - default size of inline data
#
#       iomap_size - default size of remote iomapping table
#
#       polling_time - default number of microseconds to poll for data before waiting

mkdir -p /etc/rdma/rsocket

echo 373760 > /etc/rdma/rsocket/mem_default
echo 373760 > /etc/rdma/rsocket/wmem_default
echo 128 > /etc/rdma/rsocket/sqsize_default
echo 128 > /etc/rdma/rsocket/rqsize_default
echo 256 > /etc/rdma/rsocket/inline_default
echo 128 > /etc/rdma/rsocket/iomap_size
echo 10 > /etc/rdma/rsocket/polling_time

for arg in `ls /etc/rdma/rsocket/`; do
	printf "%s\t%s\n" $arg `cat /etc/rdma/rsocket/$arg`
done
